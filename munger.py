# -*- coding: utf-8 -*-
"""
Created on Thu May 25 14:15:52 2017

@author: Oddman
"""
import codecs

#s1 = codecs.open('EU Delegation Tunisia_1.csv',encoding='windows-1252',mode='r')
s1 = open('EU Delegation Tunisia_1.csv',mode='r')

lines = s1.read().split('#####')
lines = [line.lstrip('\t') for line in lines]
lines = [line.replace('\n',' ') for line in lines]
#print lines[:2]
lines = [line.split('\t') for line in lines]

#print lines[:2]
header1 = lines[0]
lines = lines[1:-2]
cols = zip(*lines)
partners = set()
#for i in range(9,15):
#    for j in cols[i]:
#        partners.add(j)
        
#for i in sorted(list(partners)):
#    print i
partnerLookup = {}     
   
#with codecs.open('EU Delegation Tunisia_2.csv',encoding='windows-1252',mode='r') as s2:
with open('EU Delegation Tunisia_2.csv',mode='r') as s2:
    header2 = s2.readline()
    header2 = header2.split('\t')
    records2 = []
    for line in s2:
        line.replace('\n',' ')
        record = line.split('\t')
        partnerLookup[record[0]] = record[1:-1]
        
#
partnerNotFound = set()
outRecords = [header1[:8]+header1[14:]+['partner']+header2[1:-1]]
for record in lines:
    firstbit = record[:8]
    secondbit = record[14:]
    for i in range(8,14):
        partner = record[i]
        if partner != '':
            try:
#                print partner
                partnerPart = partnerLookup[partner]
#                print partnerPart
                outRecord = firstbit+secondbit+[partner]+partnerPart
                outRecords.append(outRecord)
            except KeyError:
#                print '!'
                partnerNotFound.add(partner)

    
#with codecs.open('partnerJoin.csv',encoding='windows-1252',mode='w') as outFile:
#    outFile.write('')
with open('partnerJoin.csv',mode='w') as outFile:
    outFile.write('')
    
outText = '\n'.join(['\t'.join(line) for line in outRecords])

with open('partnerJoin.csv',mode='w') as outFile:
    outFile.write(outText)


for i in sorted(list(partnerNotFound)):
    print i
                
            